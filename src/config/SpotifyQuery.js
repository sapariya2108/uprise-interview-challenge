import { SpotifyGraphQLClient } from 'spotify-graphql';
import Config from './config'

export default function SpotifyQuery(query) {

    const SpotifyConfig = {
        'clientId': Config.client_id,
        'clientSecret': Config.client_secret,
        'redirectUri': Config.redirect_url,
        'accessToken': localStorage.getItem('access_token')
    }

    return new Promise((resolve, reject) => {
        SpotifyGraphQLClient(SpotifyConfig).query(query).then((result) => {
            if (result.errors) {
                reject(JSON.stringify(result.errors))
            } else {
                resolve(result.data.user)
            }
        })
    })
}
