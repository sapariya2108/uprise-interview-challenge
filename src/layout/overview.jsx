import React, { useState, useEffect } from 'react';
import '../css/app.css';
import Header from './header'
import { Button } from '@uprise/button';

export default function Overview() {

    const [name, setName] = useState();
    const [image, setImage] = useState("");
    const [follower, setFollower] = useState(0);
    const [loading, setLoading] = useState(true)

    useEffect(() => {
        fetchOwnerDetails()
    }, [])


    //In Spotify GraphQL,Followers Count is not present
    //So I used Spotify WebAPI to get User Details
    const fetchOwnerDetails = () => {
        fetch('https://api.spotify.com/v1/users/ar4e0yqkqlci359jrc4yp0g5a', {
            method: 'GET',
            mode: 'cors',
            headers: {
                "Authorization": `Bearer ${localStorage.getItem('access_token')}`
            }
        }).then((data) => data.json())
            .then((data) => {
                let name = data.display_name;
                let image = data.images.length != 0 ? data.images[0].url : ""
                setName(name);
                setImage(image)
                setFollower(data.followers.total)
                setLoading(false);
            }).catch((err) => {
                console.log(err)
            })
    }

    return (
        <>
            <Header page="overview"></Header>
            {loading
                ? (
                    <div className="loader-card">
                        <div className="loader"></div>
                    </div>)
                : (<div style={{ margin: 'auto', marginTop: '36px', width: '970px' }}>
                    <div className="overview-card">
                        <div className="image-box">
                            <div className="owner-image">
                                <img className="image" src={image}></img>
                            </div>
                        </div>
                        <div className="owner-details">
                            <p className="name">{name}</p>
                            <p className="follower">Followers({follower})</p>
                            <Button className="follow-button" variant="primary" title="Follow" size="medium" fullWidth="true" width="120px" borderRadius="10px"></Button>
                        </div>
                    </div>
                </div>)
            }
        </>
    );
}