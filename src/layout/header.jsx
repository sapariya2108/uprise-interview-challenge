import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import '../css/app.css'

import SpotifyQuery from '../config/SpotifyQuery'

export default function Header(props) {

    const [dropDownFlag, setDropDownFlag] = useState(false);
    const [artists_array, setArtists] = useState([])
    const [loading, setLoading] = useState(true);
    const [dropDownItem, setDropDownItem] = useState([]);
    const [count, setCount] = useState(0);
    const [hoverIndex, setHoverIndex] = useState(-1)

    const history = useHistory()

    useEffect(() => {
        SpotifyQuery(`{user(id:"ar4e0yqkqlci359jrc4yp0g5a") {playlists {tracks{track{artists{name}}}}}}`).then(data => {
            const playlists = data.playlists
            fetchArtist(playlists);
            setLoading(false);
            setArtists(artists_array)
            setDropDownItem(artists_array.slice(0, 10))
            console.log(artists_array)
        }).catch((err) => {
            console.log(err)
        })
    }, [])

    //Fetch Artist name From tracks of Playlist
    const fetchArtist = (playlists) => {
        playlists.map((playlist) => {
            let playlist_obj = Object.assign({}, playlist)
            let tracks_array = [...playlist_obj.tracks]
            tracks_array.map((track) => {
                let artist = track.track.artists[0].name
                if (!artists_array.includes(artist)) {
                    artists_array.push(artist)
                }
            })
        })
    }

    //During opening the DropDown,pick the 10 value from artists_array
    //Set it to DropDownItem
    //Set True as DropDownFlag

    //During closing the DropDown,Set False as DropDownFlag
    const handleDropDown = () => {
        if (!dropDownFlag) {
            setCount(count + 1);
            let start_index = count * 10;
            if (start_index >= (artists_array.length - 10)) {
                start_index = 0;
                setCount(0);
            }
            let array = artists_array.slice(start_index, start_index + 10)
            setDropDownItem(array)
        }
        setDropDownFlag(!dropDownFlag)
    }

    const changePage = (pageName) => {
        history.replace('/' + pageName)
    }

    return (
        <div style={{ margin: 'auto', marginTop: '80px', width: '970px' }}>
            <div className="header">
                <div className="headerText" onClick={() => changePage('overview')} >
                    <p className="menuText" style={{ color: props.page == "overview" && !dropDownFlag ? '#7d60ff' : "#6d6c79" }}>Overview</p>
                </div>
                <div className="headerText" onClick={() => changePage('playlist')}>
                    <p className="menuText" style={{ color: props.page == "playlist" && !dropDownFlag ? '#7d60ff' : "#6d6c79" }}>Playlist</p>
                </div>
                <div className="headerText" onMouseEnter={handleDropDown} onMouseLeave={handleDropDown}>
                    <p className="menuText" style={{ color: dropDownFlag ? '#7d60ff' : "#6d6c79" }}>Featured</p>
                    {dropDownFlag
                        ? (<div style={{ marginTop: '5px', zIndex: 2, position: 'absolute' }}>
                            <div className="dropdown-box">
                                {loading
                                    ? (<div className="loader"></div>)
                                    : (dropDownItem.map((value, index) => (
                                        <div className="drop-down-item-card" style={{ backgroundColor: hoverIndex == index ? "#7d60ff" : "#ffffff" }} onMouseEnter={() => setHoverIndex(index)} onMouseLeave={() => setHoverIndex(-1)} >
                                            <p className="drop-down-item" style={{ color: hoverIndex == index ? "#ffffff" : "#6d6c79" }}>{value}</p>
                                        </div>
                                    )))
                                }
                            </div>
                        </div>)
                        : (<></>)
                    }
                </div>
            </div>

        </div >
    );
} 