import React from 'react'
import { useLocation, useHistory } from 'react-router-dom'


export default function Callback(props) {
    const history = useHistory();
    const query = useLocation().hash.substring(1).split('&').reduce((initial, item) => {
        if (item) {
            let parts = item.split('=')
            initial[parts[0]] = decodeURIComponent(parts[1]);
        }
        return initial;
    }, {})

    localStorage.setItem("access_token", query.access_token);

    history.replace('/overview')

    return (<></>)
}