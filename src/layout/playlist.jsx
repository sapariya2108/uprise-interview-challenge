import React, { useState, useEffect } from 'react';
import Header from './header'
import { ProgressiveImage } from '@uprise/image'
import ForwardButton from '../forward_button.png'
import BackwardButton from '../backward_button.png'
import SpotifyQuery from '../config/SpotifyQuery'
import '../css/app.css'

export default function Playlist() {

    const [playlists, setPlaylists] = useState();
    const [loading, setLoading] = useState(true);
    const [index, setIndex] = useState(0);

    useEffect(() => {
        SpotifyQuery(`{user(id:"ar4e0yqkqlci359jrc4yp0g5a") {playlists{id,name,images{url}}}}`).then(data => {
            let playlists = data.playlists;
            setPlaylists(playlists)
            setLoading(false)
        }).catch((err) => {
            console.log(err)
        })
    }, [])

    const handleBackwardButton = () => {
        if (index == 0) {
            setIndex(playlists.length - 1)
        } else {
            setIndex(index - 1)
        }
    }

    const handleForwardButton = () => {
        if (index == (playlists.length - 1)) {
            setIndex(0)
        } else {
            setIndex(index + 1)
        }
    }

    return (
        <>
            <Header page="playlist"></Header>
            {loading
                ? (
                    <div className="loader-card">
                        <div className="loader"></div>
                    </div>)
                : (<div style={{ margin: 'auto', marginTop: '36px', width: '970px' }}>
                    <div className="playlist-card">
                        <div className="song-image-button">
                            <div className="button-backward">
                                <div className="backward-button-image" onClick={handleBackwardButton}>
                                    <ProgressiveImage overlaySrc={BackwardButton} src={BackwardButton} borderRadius='10px'></ProgressiveImage>
                                </div>
                            </div>
                            <div className="song-image">
                                <img className="image" src={playlists[index].images[0].url}></img>
                            </div>
                            <div className="button-forward">
                                <div className="forward-button-image" onClick={handleForwardButton}>
                                    <ProgressiveImage overlaySrc={ForwardButton} src={ForwardButton} borderRadius='10px'></ProgressiveImage>
                                </div>
                            </div>
                        </div>
                        <div className="song-name">
                            <p className="playlist-name-text">{playlists[index].name}</p>
                        </div>
                    </div>
                </div>)
            }
        </>
    );
}