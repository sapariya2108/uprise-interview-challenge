import React from 'react';
import { Switch, Route, Redirect, BrowserRouter as Router } from 'react-router-dom'
import Overview from './layout/overview'
import Playlist from './layout/playlist'
import Callback from './layout/AuthenticationCallback'
import Config from './config/config'

function App() {
  return (
    <Router>
      <Switch>
        <Route path="/overview" render={() => isAuthenticate() ? <Overview /> : login()}></Route>
        <Route path="/playlist" render={() => isAuthenticate() ? <Playlist /> : login()}></Route>
        <Route path="/callback" render={() => <Callback />}></Route>
      </Switch>
    </Router>
  );
}


function isAuthenticate() {
  return localStorage.getItem('access_token') ? true : false
}

function login() {

  const authRequest = Config.auth_url +
    '?response_type=token' +
    '&client_id=' + Config.client_id +
    '&scope=' + Config.scope +
    '&redirect_uri=' + Config.redirect_url

  console.log('login Call', authRequest)
  window.location.href = authRequest
}

export default App;
